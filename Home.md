# Wiki apuntes de Git

## Configurar Git

Nivel de configuración:  

global (disponible para todos los repositorios del usuario actual) 	
`git config --global user.name "nombre"` <br>
`git config --global user.email "email@mail.org"` <br>

system (disponible para todos  users/repos) 
`git config --system user.name "nombre"` <br>
`git config --system user.email "email@mail.org"` <br>

repository (por defecto, solo aplicable al repo actual)
`git config user.name "nombre"` <br>
`git config user.email "email@mail.org"` <br>


## Crear un repositorio 
   `git init` <br>

## Consultar el estado del repositorio
`git status` <br>

## Añadir ficheros al repositorio

Una de las funciones centrales de Git son los conceptos de Entorno de Preparación (the stage area) y Confirmación (commit).

Mientras trabajas, puedes ir añadiendo, editando y eliminando ficheros. Pero cada vez que llegues a un hito o termines una parte del trabajo, debes añadir los ficheros a un Entorno de Preparación.

Los ficheros en preparación son ficheros que están listos para ser confirmados en el repositorio en el que se está trabajando. 

`untracked	        	  unmodified		        modified		         staged`    <br>
`    |---añadir el fichero --->|---editar el fichero--->|---stage el fichero--->|`      <br>
`    |<--eliminar el fichero-->|`                                                       <br>
`    |                         |<---------------------commit------------------->|`      <br>
		
* Stage (etapa, fase, escenario, estado, nivel, ciclo)
* Unstage (desenganchar, retirar)
* The staging area (el área de preparación)

`git add nombrefichero` <br>
`git add *.txt`   <br>
`git add . `      <br>
`git add --all`   <br>
`git add -A`      <br>

Una vez hecho un commit, si hacemos `git add Newfile.txt`, se añade al stage, si lo hicimos por error, hay dos formas de retirarlos del stage: <br>
`git reset HEAD Newfile.txt` <br> 
`git rm Newfile.txt  --cached`    (usar `--cached` para conservar el archivo, o `-f` para forzar su eliminación) <br>

## Confirmar cambios (Commit)
Añadir commits mantiene un registro de nuestros progresos y cambios a medida que trabajamos. Git considera cada commit como un punto de cambio o "punto de guardado". Es un punto en el proyecto al que puedes volver si encuentras un error o quieres hacer un cambio.

Cuando hacemos un commit, siempre debemos incluir un mensaje.

`git commit -m "Primera versión de apuntes de Git"` <br>

A veces, cuando se realizan pequeños cambios, utilizar el entorno de preparación (`git add`) parece una pérdida de tiempo. Es posible confirmar los cambios directamente, saltándose el entorno de preparación. Con la opción -a se prepararán automáticamente todos los archivos modificados y rastreados.

`git commit -a -m "Modificar fichero index.html con una línea nueva"` <br>


## Consultar el historial
`git log` <br>

Más bonito:
`git log --graph --decorate --pretty=oneline --abbrev-commit` <br>


## Ramas (branch)
Consultar la rama actual `git branch`: <br>
`usuario@maquina:~/datos/Git$ git branch` <br>
`* master` <br>
 
Crear una nueva rama (desarrollo): <br>
`usuario@maquina:~/datos/Git$ git branch desarrollo` <br>
`usuario@maquina:~/datos/Git$ git branch` <br>
`  desarrollo` <br> 
`* master` <br>
			
Cambiar de rama: <br>
`usuario@maquina:~/datos/Git$ git checkout desarrollo` <br>
`M       apuntes_git.ctb` <br>
`Cambiado a rama 'desarrollo'` <br>

## Uniendo ramas
Unir la rama desarrollo a la rama master
situarse en la rama master <br>
`git checkout master` <br>
`git merge desarrollo` <br>


## Ignorar ficheros en el repositorio
Ignorar ciertos ficheros: <br>
`.gitignore` <br>
`# ignorar ficheros temporales` <br>
`*.tmp` <br>

## Configurar remoto
`usuario@maquina:~/datos/Git/Git.wiki$ git remote -v` <br>
`origin  https://github.com/miguemoya/Git.wiki.git (fetch)` <br>
`origin  https://github.com/miguemoya/Git.wiki.git (push)` <br>


## Alias

Crear un alias para el comando anterior con git config:
`usuario@maquina:~/datos/Git$ git config --global alias.tree 'log --graph --decorate --pretty=oneline --abbrev-commit'` <br> 
`usuario@maquina:~/datos/Git$ git tree ` <br>
`* f82a7aa (HEAD -> desarrollo) nuevos apuntes de GIT en desarrollo ` <br>
`* 7249719 primer commit de la rama desarrollo ` <br>
`* 8672279 (master) Primer commit` <br>
 
`git config <level> alias.<alias name> '<your sequence of git commands>'` <br>
	level:  `--global`,  `--system`,  si no se informa nada asume repository


## Entorno gráfico
Instalar git-gui
`apt-install git-gui` <br>

Ejecutar git-gui
`git citool`  o `git gui`  <br>


## Deshacer cambios
Git Revert
Git Reset

## Stash

`git stash save “comentario opcional para documentar”`  <br>
`git stash list`  <br>
`git stash show -p stash@{0}`  <br>
`git stash apply  stash@{0}`  <br>
`git stash pop stash@{0}`  <br>
`git stash drop stash@{0}`  <br>
`git stash clear`  <br>



 




